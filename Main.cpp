#include <iostream>
#include <iomanip>
#include <string>

int main()
{
	std::string text = "Two plus two equals five";

	std::cout << text << "\n";
	std::cout << text.length() << "\n";
	std::cout << text[0] << "\n";
	std::cout << text[text.size() - 1] << "\n";
}